package tests;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TestPOM extends ProjectMethods{
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "SearchVendorName";
		testDescription = "Get the Vendor name based on Tax ID";
		author = "Govind";
		testNodes = "Vendors";
		category = "Regression";
		dataSheetName = "SearchVendor";
	}
	
	@Test(dataProvider = "fetchData")
	public void getVendorName(String emailId, String password, String taxId)
	{
		new LoginPage()
		.enterEmailId(emailId)
		.enterPassword(password)
		.clickLoginButton()
		.moveToVendorsLink()
		.clickSearchForVendorsLink()
		.enterTaxId(taxId)
		.clickSearchButton()
		.getVendorName();
	}

}
