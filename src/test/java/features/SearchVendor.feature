Feature: Search for the Vendor with Tax ID

Scenario: Search Vendor Positive scenario
Given the acme url https://acme-test.uipath.com/account/login is open in Chrome browser
And user has logged in with EmailID as tsgovindharajan@gmail.com and password as acmeGovind14
When the user navigates to Vendor Search page
And searches with Tax ID as IT145632
Then the Vendor name should be printed in the console
And close the browser

Scenario Outline: Search Vendor Positive scenario 2
Given the acme url https://acme-test.uipath.com/account/login is open in Chrome browser
And user has logged in with EmailID as tsgovindharajan@gmail.com and password as acmeGovind14
When the user navigates to Vendor Search page
And searches with Tax ID as <taxId>
Then the Vendor name should be printed in the console
And close the browser
Examples:
|taxId|
|IT145632|
|FR121212|