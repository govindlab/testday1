package tests;

import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Test1 {
	
	@Test
	public void printVendorName() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "./drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementById("email").sendKeys("tsgovindharajan@gmail.com");
		driver.findElementById("password").sendKeys("acmeGovind14");
		driver.findElementById("buttonLogin").click();
				
		WebElement btnVendors = driver.findElementByXPath("(//i[@class = 'fa fa-truck'])/..");
		Actions builder = new Actions(driver);
		builder.moveToElement(btnVendors).build().perform();
		WebElement searchVendors = driver.findElementByLinkText("Search for Vendor");
		builder.moveToElement(searchVendors).click().build().perform();
		
		driver.findElementById("vendorTaxID").sendKeys("FR322345");
		driver.findElementById("buttonSearch").click();
		String vendorName = driver.findElementByXPath("//tbody/tr[2]/td").getText();
		System.out.println(vendorName);
		
		driver.close();		
	}
}