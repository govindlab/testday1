package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorsSearchPage extends ProjectMethods {
	
	public VendorsSearchPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="vendorTaxID") WebElement txtTaxId;
	@FindBy(id="buttonSearch") WebElement btnSearch;
	
	public VendorsSearchPage enterTaxId(String taxId)
	{
		clearAndType(txtTaxId, taxId);
		return this;
	}
	
	public VendorsSearchResultsPage clickSearchButton()
	{
		click(btnSearch);
		return new VendorsSearchResultsPage();
	}

}
