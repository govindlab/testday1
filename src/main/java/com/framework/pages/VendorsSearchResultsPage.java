package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorsSearchResultsPage extends ProjectMethods{
	
	public VendorsSearchResultsPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//tbody/tr[2]/td") WebElement lblVendorName;
	
	public VendorsSearchResultsPage getVendorName()
	{
		System.out.println(getElementText(lblVendorName));
		return this;
	}
	
	public void closeBrowser()
	{
		close();
	}

}
