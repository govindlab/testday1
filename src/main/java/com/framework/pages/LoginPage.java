package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LoginPage extends ProjectMethods
{
	
	public LoginPage()
	{
		PageFactory.initElements(driver, this);		
	}
	
	@FindBy(id = "email") WebElement txtEmailId;
	@FindBy(id = "password") WebElement txtPassword;
	@FindBy(id = "buttonLogin") WebElement btnLogin;
	
	public LoginPage enterEmailId(String emailId)
	{
		clearAndType(txtEmailId, emailId);
		return this;
	}
	
	public LoginPage enterPassword(String password)
	{
		clearAndType(txtPassword, password);
		return this;
	}
	
	public DashboardPage clickLoginButton()
	{
		click(btnLogin);
		return new DashboardPage();
	}
}