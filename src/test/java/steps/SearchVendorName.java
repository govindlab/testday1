package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchVendorName {
	public ChromeDriver driver;
	
	@Given("the acme url (.*) is open in Chrome browser")
	public void openURL(String url)
	{
		System.setProperty("webdriver.chrome.driver", "./drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(url);		
	}
	
	@Given("user has logged in with EmailID as (.*) and password as (.*)")
	public void login(String emailId, String password)
	{
		driver.findElementById("email").sendKeys(emailId);
		driver.findElementById("password").sendKeys(password);
		driver.findElementById("buttonLogin").click();
	}
	
	@When("the user navigates to Vendor Search page")
	public void goToVendorSearch()
	{	
		Actions builder = new Actions(driver);
		WebElement btnVendors = driver.findElementByXPath("(//i[@class = 'fa fa-truck'])/..");
		builder.moveToElement(btnVendors).build().perform();		
		WebElement searchVendors = driver.findElementByLinkText("Search for Vendor");
		builder.moveToElement(searchVendors).click().build().perform();
	}
	
	@When("searches with Tax ID as (.*)")
	public void searchVendorName(String taxId)
	{
		driver.findElementById("vendorTaxID").sendKeys(taxId);
		driver.findElementById("buttonSearch").click();
	}
	
	@Then("the Vendor name should be printed in the console")
	public void printVendorName()
	{		
		System.out.println(driver.findElementByXPath("//tbody/tr[2]/td").getText());
	}
	
	@Then("close the browser")
	public void closeBrowser()
	{
		driver.close();
	}

}
