package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DashboardPage extends ProjectMethods{
	
	public DashboardPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="(//i[@class = 'fa fa-truck'])/..") WebElement lnkVendors;
	@FindBy(linkText="Search for Vendor") WebElement lnkSearchForVendors;
	
	public DashboardPage moveToVendorsLink()
	{
		moveToElement(lnkVendors);
		return this;
	}
	
	public VendorsSearchPage clickSearchForVendorsLink()
	{
		moveAndClikElement(lnkSearchForVendors);
		return new VendorsSearchPage();
	}
}